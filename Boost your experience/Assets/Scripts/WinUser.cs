﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WinUser
{
    public string winText; 

    public WinUser(string win)
    {
        winText = win;
    }
}
