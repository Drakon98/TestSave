﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Save
{
    public Level Level;
    public int Count;

    public Save(Level level, int count)
    {
        Level = level;
        Count = count;
    }
}
