﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level", order = 0)]
[Serializable]
public class Level : ScriptableObject
{
    public int NumberLevel;
    public int MaxScore;
    public int Step;

    public Level(int numberLevel, int maxScore, int step)
    {
        NumberLevel = numberLevel;
        MaxScore = maxScore;
        Step = step;
    }
}
