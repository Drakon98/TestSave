﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    public Text Level;
    public Text Count;
    public Text MaxExperience;
    public Slider Experience;

    private void Awake()
    {
        if (Saver.isLoad)
        {
            string json = File.ReadAllText($"{Application.persistentDataPath}\\Save.json");
            var save = JsonUtility.FromJson<Save>(json);
            Level.text = save.Level.NumberLevel.ToString();
            Count.text = save.Count.ToString();
            MaxExperience.text = save.Level.MaxScore.ToString();
            Experience.value = Convert.ToInt32(Count.text);
            Experience.maxValue = save.Level.MaxScore;
        }
    }
}
