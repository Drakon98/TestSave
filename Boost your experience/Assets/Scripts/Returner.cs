﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Returner : MonoBehaviour
{
    public Text NumberLevel;
    public Text Count;

    public void Return()
    {
        var CurrentLevel = Resources.Load<Level>($"Levels\\Level{NumberLevel.text}");
        var save = new Save(CurrentLevel, Convert.ToInt32(Count.text));
        var json = JsonUtility.ToJson(save);
        File.WriteAllText($"{Application.persistentDataPath}\\Save.json", json);
        SceneManager.LoadScene(0);
    }
}
