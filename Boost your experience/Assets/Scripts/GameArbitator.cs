﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameArbitator : MonoBehaviour
{
    public GameObject LevelObject;
    public Text NumberLevel;
    public Text Count;
    public Text Win;
    public Slider Experience;
    public Text MaxExperience;
    public GameObject Plus;
    public int MaxLevel;
    public UnityEvent NextLevel = new UnityEvent();
    public UnityEvent YouWin = new UnityEvent();
    private Level CurrentLevel;

    void Start()
    {
        LoadLevel();
        NextLevel.AddListener(LoadNextLevel);
        YouWin.AddListener(WinInvoke);
    }

    private void LoadLevel()
    {
        CurrentLevel = Resources.Load<Level>($"Levels\\Level{NumberLevel.text}");
    }

    void Update()
    {
        if (CurrentLevel.MaxScore == Convert.ToInt32(Count.text) && CurrentLevel.NumberLevel == MaxLevel)
        {
            YouWin.Invoke();
        }
        else if (CurrentLevel.MaxScore == Convert.ToInt32(Count.text))
        {
            NextLevel.Invoke();
        }
    }

    public void LoadNextLevel()
    {
        Resources.UnloadAsset(CurrentLevel);
        NumberLevel.text = (Convert.ToInt32(NumberLevel.text) + 1).ToString();
        LoadLevel();
        Count.text = "0";
        Experience.value = 0;
        MaxExperience.text = CurrentLevel.MaxScore.ToString();
        Experience.maxValue = CurrentLevel.MaxScore;
    }

    public void WinInvoke()
    {
        if (Convert.ToInt32(Count.text) == CurrentLevel.MaxScore)
        {
            LevelObject.SetActive(false);
            Experience.enabled = false;
            var textAsset = Resources.Load<TextAsset>("Win");
            var winUser = JsonUtility.FromJson<WinUser>(textAsset.text);
            Win.text = winUser.winText;
            Plus.SetActive(false);
        }
    }

    public void Sum()
    {
        Count.text = (Convert.ToInt32(Count.text) + CurrentLevel.Step).ToString();
        Experience.value += CurrentLevel.Step;
    }
}
