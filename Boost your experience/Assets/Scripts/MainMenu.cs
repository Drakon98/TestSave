﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Button load;

    private void Start()
    {
        if (!File.Exists($"{Application.persistentDataPath}\\Save.json"))
            load.interactable = false;
        else
            load.interactable = true;
    }

    public void StartGame()
    {
        Saver.isLoad = false;
        SceneManager.LoadScene(1);
    }

    public void LoadGame()
    {
        Saver.isLoad = true;
        SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
